import gulp                from 'gulp';
import yargs               from 'yargs';
import sass                from 'gulp-sass';
import cleanCss            from 'gulp-clean-css';
import gulpif              from 'gulp-if';
import sourcemaps          from 'gulp-sourcemaps';
import svgmin              from 'gulp-svgmin';
import tinypng             from 'gulp-tinypng';
import del                 from 'del';
import webpack             from 'webpack-stream';
import uglify              from 'gulp-uglify';
import named               from 'vinyl-named';
import browserSync         from 'browser-sync';
import zip                 from 'gulp-zip';
import replace             from 'gulp-replace';
import concat              from 'gulp-concat';
import info                from './package.json';

const server = browserSync.create();
const PRODUCTION = yargs.argv.prod;

const paths = {
	styles: {
		src:  'src/scss/bundle.scss',
		dest: 'dist/css'
	},
	stylesdep: {
		src:  ['node_modules/bootstrap/dist/css/bootstrap.min.css', 'node_modules/swiper/swiper-bundle.min.css'],
		dest: 'dist/css'
	},
	images: {
		src:  'src/images/**/*.{jpg,jpeg,png,gif}',
		dest: 'dist/images'
	},
	svg: {
		src:  'src/images/**/*.svg',
		dest: 'dist/images'
	},
	scrips: {
		src:  'src/js/bundle.js',
		dest: 'dist/js'
	},
	scriptsdep: {
		src:  ['node_modules/jquery/dist/jquery.min.js', 'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js', 'node_modules/@popperjs/core/dist/umd/popper.min.js', 'node_modules/eva-icons/eva.min.js', 'node_modules/swiper/swiper-bundle.min.js', 'node_modules/jquery-lazy/jquery.lazy.min.js'],
		dest: 'dist/js'
	},
	other: {
		src: ['src/**/*','!src/{images,js,scss}','!src/{images,js,scss}/**/*'],
		dest: 'dist/'
	},
	copylg: {
		src:  'node_modules/lightgallery/dist/fonts/*',
		dest: 'dist/fonts'
	},
	package: {
		src: [
		      "**/*",
		      "!.vscode",
		      "!node_modules{,/**}",
		      "!packaged{,/**}",
		      "!src{,/**}",
		      "!.babelrc",
		      "!.gitignore",
		      "!gulpfile.babel.js",
		      "!package.json",
		      "!package-lock.json"
    	],
		dest: 'packaged'
	}
}

export const serve = (done) => {
	server.init({
		proxy: "http://localhost/dev-fullstack-php-2021"
	});
	done();
}

export const reload = (done) => {
	server.reload();
	done();
}

export const clean = () => {
	return del(['dist']);
}

export const styles = (done) => {
	return gulp.src(paths.styles.src)
		.pipe(gulpif(!PRODUCTION, sourcemaps.init()))
		.pipe(sass().on('error',sass.logError))
		.pipe(gulpif(PRODUCTION, cleanCss({compatibility:'ie8'})))
		.pipe(gulpif(!PRODUCTION, sourcemaps.write()))
		.pipe(gulp.dest(paths.styles.dest))
		.pipe(server.stream());
}

export const stylesdep = () => {
  return gulp.src(paths.stylesdep.src)
      .pipe(cleanCss({compatibility:'ie8'}))
      .pipe(concat('dep.css'))
      .pipe(gulp.dest(paths.stylesdep.dest))
}


export const images = () => {
  return gulp.src(paths.images.src)
    .pipe(gulpif(PRODUCTION, tinypng('p2NGQ4GZ7wDA1CG1EspGcXATRah2T5a3')))
    .pipe(gulp.dest(paths.images.dest));
}

export const svg = () => {
    return gulp.src(paths.svg.src)
      .pipe(gulpif(PRODUCTION, svgmin()))
      .pipe(gulp.dest(paths.svg.dest));
}

export const watch = () => {
	gulp.watch('src/scss/**/*.scss', styles);
	gulp.watch('src/js/**/*.js', gulp.series(scripts, reload));
	gulp.watch('**/*.php', reload);
	gulp.watch(paths.images.src, gulp.series(images, svg, reload));
	gulp.watch(paths.other.src, gulp.series(copy, reload));
} 


export const copy = () => {
	return gulp.src(paths.other.src)
		.pipe(gulp.dest(paths.other.dest));
}

export const lgfiles = () => {
	return gulp.src(paths.copylg.src)
		.pipe(gulp.dest(paths.copylg.dest));
}

export const scripts = () => {
	return gulp.src(paths.scrips.src)
	.pipe(named())
	.pipe(webpack({
		module: {
			rules: [
				{
					test: /\.js$/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env']
						}
					}
				}
			]
		},
		output: {
			filename: '[name].js'
		},
		devtool: !PRODUCTION ? 'inline-source-map' : false
	}))
	.pipe(gulpif(PRODUCTION, uglify()))
	.pipe(gulp.dest(paths.scrips.dest));
}

export const scriptsdep = () => {
	return gulp.src(paths.scriptsdep.src)
		.pipe(concat('dep.js'))
		.pipe(gulpif(PRODUCTION, uglify()))
		.pipe(gulp.dest(paths.scriptsdep.dest))
}

export const compress = () => {
	return gulp.src(paths.package.src)
		.pipe(replace('phidelis-core', info.name))
		.pipe(zip(`${info.name}.zip`))
		.pipe(gulp.dest(paths.package.dest));
}

export const dev = gulp.series(clean, gulp.parallel(styles, stylesdep, images, svg, copy, lgfiles, scriptsdep, scripts), serve, watch);
export const build = gulp.series(clean, gulp.parallel(styles, stylesdep, images, svg, copy, lgfiles, scriptsdep, scripts));
export const bundle = gulp.series(build,compress);
export default dev;