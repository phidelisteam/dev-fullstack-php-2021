# 💻 Teste Full Stack PHP - Phidelis Tecnologia

[Layout](https://xd.adobe.com/view/5c3079dc-4d24-4d9c-8bb1-004437b6f107-0963/)

### Descrição Geral

O teste consiste no desenvolvimento de um site para uma agência de viagens onde o cliente (agência) terá uma area administrativa podendo assim cadastrar determinados itens tornando o site dinâmico. Disponibilizaremos o layout em Adobe XD para orientação visual do que se espera no resultado final. 

Desta forma serão avaliadas as habilidades do candidato na linguagem PHP observando lógica, domínio, padrões de projetos, organização, performance, velocidade na entrega, qualidade do código, análise e compreensão do escopo, assim como validar seus conhecimentos na integração entre interfaces front-end e back-end.

Após  finalização do teste realizar um pull request no repositório para validarmos o código.


### Requisitos

- Arquitetura MVC
- Pode utilizar o Framework Laravel
- NÃO utilizar query builders
- NÃO utilizar migrations
- Utilizar PHP 7.*
- PHP Orientado a Objetos
- [PSRs](https://www.php-fig.org/psr/)
- HTML5
- CCS3
- JavaScript
- Git
- MySQL

### 💡 Diferenciais

- Desenvolver framework próprio (Respeitando as regras de requisitos)
- Utilizar Template Engine
- Utilizar Ajax na inserção e atualização dos cadastros e na busca
- Utilizar Design Patterns
- Clean Code

## Configuração do projeto
> Siga o passo-a-passo para configurar o projeto na sua máquina

### Pré-requisitos

- [Node.js](https://nodejs.org/en/download/)
  - Instale a versão (LTS)
- [Gulp](http://gulpjs.com/)
  - Instale globalmente utilizando o comando `npm install --global gulp-cli` no terminal

### Configuração inicial do projeto

1. Navegue até o diretório-raiz do seu projeto no Terminal (para usuários windows recomendo que [leiam este artigo](https://www.felipefialho.com/blog/2017/usando-o-terminal-do-linux-no-windows))
2. Execute o comando `npm install`
	- Este comando irá instalar todas as dependências do projeto que estão listadas no package.json
	- Pode levar alguns minutos
	- Irá produzir muitas informações de depuração (incluindo avisos ocasionais de depreciação) que podem ser ignoradas
3. Execute o comando `npm run start`
	- Este comando irá executar todas as tarefas estabelecidas no gulp para visualização do projeto. (o gulpfile já está configurado, não necessita de alterações)

### Comandos comuns
- `npm run start` - executa e monitora a compilação dos arquivos em src (scss, js, fonts, img)

## Arquivos fonte
### Sass (scss)

`bundle.scss` é o arquivo Sass de nível superior, que normalmente apenas importa os arquivos scss para serem compilados.

#### _variables.scss

Contém todas as variáveis, desde tamanhos de fonte até cores, para facilitar o tempo das funções.

## Plugins e frameworks
Nós incluímos o usual aqui, que comumente utilizamos para agilizar os processos.
- [bootstrap 5.0.0](https://getbootstrap.com/)
- [Swiper 6.4.12](https://idangero.us/swiper/)
- [Eva Icons](https://akveo.github.io/eva-icons/)
- [tinymce](https://www.tiny.cloud/docs/demo/basic-example/)


## Tipografia

Por padrão a fonte inicial utilizada é a **Axiforma** e já está implementada no projeto

